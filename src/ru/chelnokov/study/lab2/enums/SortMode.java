package ru.chelnokov.study.lab2.enums;

/**
 * Перечисление доступных режимов сортировки, а именно:
 *
 * по возрастанию значений;
 * по убыванию значений.
 */
public enum SortMode {

    ASC("ascending"),
    DESC("descending");
    String modeName;

    SortMode(String modeName) {
        this.modeName = modeName;
    }

    public String getModeName() {
        return modeName;
    }
}
