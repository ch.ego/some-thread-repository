package ru.chelnokov.study.lab2.types;


import ru.chelnokov.study.lab2.enums.SortMode;

public class InsertionSorter extends ArraySorter {

    public InsertionSorter(String[] array) {
        super("Insertion sort", array);
    }

    @Override
    public String[] sort(SortMode mode) {
        for (int left = 0; left < array.length; left++) {
            String value = array[left];
            int i = left - 1;

            for (; i >= 0; i--) {
                if (((value.compareTo(array[i]) < 0) && (mode == SortMode.ASC)) ||
                        ((value.compareTo(array[i]) > 0) && (mode == SortMode.DESC))) {
                    array[i + 1] = array[i];
                } else {
                    break;
                }
            }
            array[i + 1] = value;
        }
        return array;
    }
}
