package ru.chelnokov.study.lab2.types;


import ru.chelnokov.study.lab2.enums.SortMode;

public class SelectionSorter extends ArraySorter {
    public SelectionSorter(String[] array) {
        super("Selection sort", array);
    }

    @Override
    public String[] sort(SortMode mode) {
        for (int left = 0; left < array.length; left++) {
            int minInd = left;
            for (int i = left; i < array.length; i++) {
                if (((array[i].compareTo(array[minInd]) < 0) && (mode == SortMode.ASC)) ||
                        ((array[i].compareTo(array[minInd]) > 0) && (mode == SortMode.DESC))) {
                    minInd = i;
                }
            }
            swap(array, left, minInd);
        }
        return array;
    }
}
