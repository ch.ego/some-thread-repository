package ru.chelnokov.study.lab2.types;


import ru.chelnokov.study.lab2.enums.SortMode;

public class QuickSorter extends ArraySorter {
    public QuickSorter(String[] array) {
        super("Quick sort", array);
    }

    @Override
    public String[] sort(SortMode mode) {
        int leftBorder = 0;
        int rightBorder = array.length - 1;
        recursionSort(array, leftBorder, rightBorder, mode == SortMode.ASC);
        return array;
    }

    private void recursionSort(String[] array, int leftBorder, int rightBorder, boolean isAscMode) {
        int leftMarker = leftBorder;
        int rightMarker = rightBorder;
        String pivot = array[(leftMarker + rightMarker) / 2];
        do {
            //сдвиг границ
            while (((array[leftMarker].compareTo(pivot) < 0 && isAscMode) || ((array[leftMarker].compareTo(pivot) > 0) && !isAscMode))) {
                leftMarker++;
            }
            while (((array[rightMarker].compareTo(pivot) > 0) && isAscMode) || ((array[rightMarker].compareTo(pivot) < 0) && !isAscMode)) {
                rightMarker--;
            }

            if (leftMarker <= rightMarker) {
                if (leftMarker < rightMarker) {
                    String tmp = array[leftMarker];
                    array[leftMarker] = array[rightMarker];
                    array[rightMarker] = tmp;
                }
                // повторный сдвиг
                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);

        // Выполняем рекурсивно для частей
        if (((leftMarker < rightBorder) && isAscMode) || ((leftMarker > rightBorder) && !isAscMode)) {
            recursionSort(array, leftMarker, rightBorder, isAscMode);
        }
        if (((leftBorder < rightMarker) && isAscMode) || ((leftBorder > rightMarker) && !isAscMode)) {
            recursionSort(array, leftBorder, rightMarker, isAscMode);
        }
    }
}
