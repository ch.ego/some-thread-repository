package ru.chelnokov.study.lab2.types;

import ru.chelnokov.study.lab2.enums.SortMode;

/**
 * Абстрактный класс вида сортировки целочисленного массива
 *
 */
public abstract class ArraySorter {
    String name;
    String[] array;

    public ArraySorter(String name, String[] array) {
        this.name = name;
        this.array = array;
    }

    public abstract String[] sort(SortMode mode);

    public String getName() {
        return name;
    }

    /**
     * меняет значения двух элементов массива между собой
     *
     * @param array  исходный массив
     * @param index1 индекс первого элемента
     * @param index2 индекс второго элемента
     */
    void swap(String[] array, int index1, int index2) {
        String temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }
}
