package ru.chelnokov.study.lab2;


import ru.chelnokov.study.lab2.enums.SortMode;
import ru.chelnokov.study.lab2.types.*;

import java.util.Arrays;

public class Main {
    final static String[] NOMBRES = {"Un", "Deux", "Trois", "Quatre", "Cinq", "Six", "Sept", "Huit", "Neuf", "Dix"};
    public static void main(String[] args) {
        System.out.println("Source array: " + Arrays.toString(NOMBRES));

        Thread quick = new Thread(() -> doRun(new QuickSorter(NOMBRES.clone())));
        Thread selection = new Thread(() -> doRun(new SelectionSorter(NOMBRES.clone())));
        Thread insertion = new Thread(() -> doRun(new InsertionSorter(NOMBRES.clone())));

        quick.start();
        selection.start();
        insertion.start();
    }

    private static void doRun(ArraySorter created) {
        System.out.println(created.getName() + "\nSorted array: " +
                Arrays.toString(created.sort(SortMode.ASC)));
    }
}
