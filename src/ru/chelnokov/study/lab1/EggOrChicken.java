package ru.chelnokov.study.lab1;

import static java.lang.Thread.sleep;

public class EggOrChicken {

    private static volatile String result = "who knows...";

    public static void main(String[] args) throws InterruptedException {
        Thread chicken = new Thread(() -> say("-CHICKEN!"));
        Thread egg = new Thread(() -> say("-EGG!"));

        System.out.println("DISCUSSION STARTED");
        chicken.start();
        egg.start();

        if (chicken.isAlive()) {
            chicken.join();
        }
        if (egg.isAlive()) {
            egg.join();
        }
        System.out.println("THE RESULT IS: " + result);
    }

    private static void say(String opinion) {
        for (int i = 0; i < 5; i++) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
            System.out.println(opinion);
            result = opinion;
        }
    }
}
